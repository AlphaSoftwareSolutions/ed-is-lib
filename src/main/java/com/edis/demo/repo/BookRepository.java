package com.edis.demo.repo;

import com.edis.demo.model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends CrudRepository<Book, Integer> {

    List<Book> findByNumberOfCopiesGreaterThan(Integer numberOfCopies);

    Optional<Book> findById(Integer id);

}
