package com.edis.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    public static NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();

    }

    @Override
    public void configure(AuthenticationManagerBuilder web) throws Exception {
        web.inMemoryAuthentication().withUser("citizen").password("123").roles("CITIZEN");
        web.inMemoryAuthentication().withUser("librarian").password("123").roles("LIBRARIAN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/library/**").permitAll()
                .antMatchers("/citizen/**").hasRole("CITIZEN")
                .antMatchers("/librarian/**").hasRole("LIBRARIAN")
                .and()
                .httpBasic();


    }
}
