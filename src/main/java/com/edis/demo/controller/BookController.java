package com.edis.demo.controller;

import com.edis.demo.error.BookNotAvailableException;
import com.edis.demo.error.BookNotFoundException;
import com.edis.demo.model.Book;
import com.edis.demo.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
public class BookController {

    @Autowired
    private BookRepository bookRepo;

    @GetMapping(value = "/library/", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Book> index() {
        Iterable<Book> bookIterable = bookRepo.findAll();
        List<Book> bookList = new ArrayList<>();
        bookIterable.forEach(bookList::add);
        return bookList;
    }


    @GetMapping(value = "/library/listAvailable", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Book> listAvailable() {
        Iterable<Book> bookIterable = bookRepo.findByNumberOfCopiesGreaterThan(0);
        List<Book> bookList = new ArrayList<>();
        bookIterable.forEach(bookList::add);
        return bookList;
    }

    @GetMapping(path = "citizen/borrow/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Book borrowBook(@PathVariable Integer id) {
        Book book = bookRepo.findById(id).orElseThrow(() -> new BookNotFoundException(id));
        Integer numberOfCopies = book.getNumberOfCopies();
        if (numberOfCopies == 0) throw new BookNotAvailableException();
        book.setNumberOfCopies(numberOfCopies - 1);
        bookRepo.save(book);
        return book;
    }


    @GetMapping(value = "citizen/return/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Map<String, String> returnBook(@PathVariable Integer id) {
        Book bookOptional = bookRepo.findById(id).orElseThrow(() -> new BookNotFoundException(id));
        Integer numberOfCopies = bookOptional.getNumberOfCopies();
        bookOptional.setNumberOfCopies(numberOfCopies + 1);
        bookRepo.save(bookOptional);
        return Collections.singletonMap("response", "Returned");
    }

    @GetMapping(value = "librarian/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> newBook(@RequestBody Book book) {
        bookRepo.save(book);
        return Collections.singletonMap("response", "New Book is saved");
    }

    @GetMapping(value = "librarian", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> welcomeLibrarian() {
        return Collections.singletonMap("response", "Welcome Librarian");
    }
}
