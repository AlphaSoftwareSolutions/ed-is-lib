package com.edis.demo;

import com.edis.demo.model.Book;
import com.edis.demo.repo.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(BookRepository bookRepo) {
        return args -> {
            if (bookRepo.count() == 0) {
                final Book book1 = new Book(1, "Spring Boot - MongoDB + Data + Web", 10);
                final Book book2 = new Book(2, "Spring Boot - Data + Web", 0);
                final Book book3 = new Book(3, "Spring Cloud - Zuul + Eureka + Rest Web", 6);

                bookRepo.save(book1);
                bookRepo.save(book2);
                bookRepo.save(book3);
            }
        };
    }
}
