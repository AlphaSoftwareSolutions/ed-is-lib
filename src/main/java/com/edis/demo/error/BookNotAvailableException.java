package com.edis.demo.error;

public class BookNotAvailableException extends RuntimeException {

    public BookNotAvailableException() {
        super("Book is not available : 0 items left");
    }
}
