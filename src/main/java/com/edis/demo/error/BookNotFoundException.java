package com.edis.demo.error;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(Integer id) {
        super("Book id not found : " + id);
    }
}
